global find_word

%include "lib.inc"

section .text

find_word:
    push r12
    push r13
    push r14

    mov r12, rdi            ; string pointer
    mov r13, rsi            ; dictionary pointer
    
    .loop:
        mov  r14, [r13]     ; load next item address
        add  r13, 8         ; pont to key
        mov  rdi, r12
        mov  rsi, r13
        call string_equals
        je   .success
    .fail:
        mov  rax, r14
        test rax, rax       ; next item address == 0? 
        jz   .ret           ; yes - ret
        mov  r13, r14       ; no - loop
        jmp  .loop
.success:
    sub  r13, 8
    mov  rax, r13
.ret:
    pop r14
    pop r13
    pop r12
    ret
    
