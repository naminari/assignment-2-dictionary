global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define SPACE 0x20
%define TAB 0x9
%define NL 0xA

section .data
  ERROR db 'Ошибка', 0

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor  rax, rax
counter:
    cmp  byte[rdi+rax], 0
    je   end
    inc  rax
    jmp  counter
end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rdi
  mov rdx, rax
  mov rax, 1
  mov rsi, rdi
  mov rdi, 1
  mov rdi, 1
    syscall           ; вызов write
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    call print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor rdx, rdx
    xor rsi, rsi

    push rbx
    mov rbx, rsp
    mov rax, rdi
    mov rsi, 10
    mov [rsp], byte 0

    .loop:
        div rsi

        lea rdx, ['0' + rdx]
        dec rsp
        mov [rsp], dl
        xor rdx, rdx
        test rax, rax
        jnz .loop

    mov rdi, rsp
    call print_string

    mov rsp, rbx
    pop rbx
    ret
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    test rax, rax
    jns .print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

    .print_uint:
     jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе

string_equals:
    xor rax, rax
    xor rcx, rcx

    .loop:
        mov al, byte [rdi + rcx]
        cmp al, byte [rsi + rcx]
        jne .fls
        cmp byte [rdi + rcx], 0
        je .tr
        cmp byte [rsi + rcx], 0
        je .tr
        inc rcx
        jmp .loop

    .fls:
        xor rax, rax
        ret

    .tr:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0      ; устанавливаем номер системного вызова "read" (0)
    mov rdi, 0      ; устанавливаем дескриптор на stdin (0)
    sub rsp, 16     ; уменьшаем указатель стека на 16
    mov rsi, rsp    ; приравниваем адрес символа и указатель стека
    mov rdx, 1      ; устанавливаем длину символа в байтах (1)
    syscall

    test rax, rax    ; проверка rax на 0 и отрицательное значение
    js .error        ; если отрицательное, переходим к обработке ошибки

    jz .end          ; если равен 0, то дошли до конца потока, возвратим rax=0

    mov al, byte[rsp]  ; загрузка символа в al
.end:
    add rsp, 16      ; возвращаем указателю стека первоначальное значение
    ret

.error:
    mov rdi, ERROR   ; устанавливаем адрес строки 'Ошибка'
    mov rax, 1       ; устанавливаем номер системного вызова "write" (1)
    mov rdx, 7       ; устанавливаем длину строки (7 байт)
    syscall
    
    mov rax, -1      ; установка кода ошибки (можно выбрать другой подходящий код)
    add rsp, 16      ; возвращаем указателю стека первоначальное значение
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
  push r12
  push r13
  push r14
  mov r12, rdi
  mov r13, rsi
  mov r14, 0

.loop:
  call read_char
  test rax, rax
  jz .end
  cmp r14, r13
  jnl .err
  cmp rax, SPACE
  jz .skip
  cmp rax, TAB
  jz .skip
  cmp rax, NL
  jz .skip
  mov [r12 + r14], al
  inc r14
  jmp .loop

.skip:
  test r14, r14
  jz .loop

.end:
  mov byte[r12 + r14], 0
  mov rax, r12
  mov rdx, r14
  jmp .exit

.err:
  mov rax, 0

.exit:
  pop r14
  pop r13
  pop r12
  ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx

    .read_loop:
        movzx rcx, byte [rdi + rdx]

        test rcx, rcx
        jz .end

        cmp rcx, '0'
        jb .not_digit
        cmp rcx, '9'
        ja .not_digit

        sub rcx, '0'


imul rax, rax, 10
        add  rax, rcx

        inc  rdx
        jmp  .read_loop

    .not_digit:
        test rax, rax
        jnz .end
        xor rdx, rdx

    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .neg
    cmp byte[rdi], '+'
    je .pos

    .none:
        jmp parse_uint

    .neg:
        inc rdi
        call parse_uint

    inc rdx
    neg rax
    ret
    .pos:
        inc rdi

    jmp parse_uint

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
            cmp rax, rdx
            jz .zero
            mov r9b, [rdi + rax]
            mov [rsi + rax], r9b
            inc rax
            test r9b, r9b
            jz end
            jmp .loop

    .zero:
            xor rax, rax

    .end:
            ret
