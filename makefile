ASM=nasm
ASMFLAGS=-f elf64
LD=ld
PYTHON=python3

all: clean main

main: main.o dict.o lib.o
	$(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	rm *.o

test: all
	$(PYTHON) test.py

.PHONY: all clean test
