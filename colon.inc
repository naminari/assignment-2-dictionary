%define link 0

%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
            dq link
            db %1, 0
            %define link %2
        %else
            %error "En el argumento 2, se necesita un identificador"
        %endif
    %else
        %error "El argumento 1 es un error!"
    %endif
%endmacro
