import subprocess

# Predefined input, expected stdout, and expected stderr
predefined_inputs = ["", "primero_palabra", "tercera_palabra", "ninguna_palabra", "ratatatattattatatatatatatatatatatatatatatatatatatatatatatatatatattattataattatatatatatatatatatatatatattatatatatatatatatatatatatatatatatatatatttatatatatatatatatatatatatatatatatattatatatatatatatatatatatatatatatatatatatatattatatatatatatatattatatatatatatattatatatatatatattatatatatatat"]
expected_stdouts = ["", "explicación de la primera palabra", "explicación de la tercera palabra", "", ""]
expected_stderrs = ["error: Key not found", "", "", "error: Key not found", "error: Key is too long"]
correct_runs = 0

for i in range(len(predefined_inputs)):
    user_input = predefined_inputs[i]
    expected_stdout = expected_stdouts[i]
    expected_stderr = expected_stderrs[i]

    process = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    stdout, stderr = process.communicate(input=user_input)
    stdout, stderr = stdout.strip(), stderr.strip()

    print(f"Run {i + 1} - ", end="")

    if stdout == expected_stdout and stderr == expected_stderr:
        print(f"BRAVO!")
        print(f"Input: {user_input}")
        print(f"Stdout: {stdout}")
        print(f"Stderr: {stderr}")
        correct_runs += 1
    else:
        print(f"INCORRECTO")
        print(f"Input: {user_input}")

        if stdout != expected_stdout:
            print(f"Expected Stdout: {expected_stdout}")
            print(f"Actual Stdout: {stdout}")
        else:
            print(f"Stdout: {stdout}")

        if stderr != expected_stderr:
            print(f"Expected Stderr: {expected_stderr}")
            print(f"Actual Stderr: {stderr}")
        else:
            print(f"Stderr: {stderr}")

    print("^^^^^^^")

print(f"número de pruebas completadas: {correct_runs}/{len(predefined_inputs)}")
